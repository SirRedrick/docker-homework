import React from 'react';

export default function TryAgainButton({ onClick }) {
	return <button onClick={onClick}>Try Again</button>;
}
