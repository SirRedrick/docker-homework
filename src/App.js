import React, { Fragment } from 'react';
import Game from './components/containers/Game';
import './App.css';

export default function App() {
	return (
		<Fragment>
			<Game />
		</Fragment>
	);
}
